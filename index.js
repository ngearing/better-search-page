//@ts-check
const Bundler = require('parcel-bundler')
const bs = require('browser-sync').create()
const proxy = require('http-proxy-middleware')
const path = require('path')
const entryFiles = './src/*.js'

;(async function() {
	const bundler = new Bundler(entryFiles, {
		hmrHostname: 'localhost'
	})
	const bundle = await bundler.bundle()

	// bs.init({
	// 	open: false,
	// 	files: ['*.php', '**/*.php'],
	// 	proxy: 'https://local.meaningfulageing.org.au/',
	// 	middleware: [
	// 		proxy('/', {
	// 			target: 'https://local.meaningfulageing.org.au',
	// 			changeOrigin: true
	// 		}),
	// 		bundler.middleware()
	// 	]
	// })
})()
