module.exports = {
	proxy: 'https://local.meaningfulageing.org.au/',
	files: ['*.php', 'lib/*.php', 'templates']
}
