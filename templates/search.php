<?php
get_header();

global $s, $wp_query;
?>

<main class="page search-page">
	<div class="container">

		<?php get_search_form(); ?>

		<h1>Search Results</h1>
		<?php printf( '<h2>Found %d results containing the term "%s"</h2>', $wp_query->found_posts, $s ); ?>


		<?php bsp_paginate(); ?>

		<div class="search-results">

			<?php if ( have_posts() ) : ?>

				<?php bsp_new_search_section( true ); ?>

				<?php
				while ( have_posts() ) :
					the_post();
					?>

					<?php bsp_new_search_section(); ?>

					<?php bsp_search_item(); ?>

					<?php
				endwhile;
				?>

				<?php bsp_close_search_section(); ?>

			<?php else : ?>

				<section class="search-section search-section-no-results">
					<h2 class="search-section-title">No results found.</h2>
				</section>

			<?php endif; ?>

		</div>

		<?php bsp_paginate(); ?>

	</div>
</main>

<?php
get_footer();
