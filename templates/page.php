<?php

global $post, $bsp_pt;
?>

<article id="post-<?php the_id(); ?>" <?php post_class( [ 'search-item', "search-item-$bsp_pt" ] ); ?>>

	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

</article>
