<form id="search-form">

	<label for="s">Search for:</label>
	<input type="text" id="s" name="s" placeholder="Search for...">
	<button type="submit">Search</button>

</form>

<form role="search" ' . $aria_label . 'method="get" class="search-form" action="' . esc_url( home_url( '/' ) ) . '">
	<label>
		<span class="screen-reader-text">' . _x( 'Search for:', 'label' ) . '</span>
		<input type="search" class="search-field" placeholder="' . esc_attr_x( 'Search &hellip;', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" />
	</label>
	<input type="submit" class="search-submit" value="' . esc_attr_x( 'Search', 'submit button' ) . '" />
</form>