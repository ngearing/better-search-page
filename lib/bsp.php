<?php
/**
 *
 */

namespace BSP;

class BSP {

	var $name = 'plugin-name';

	public function __construct( $name ) {
		$this->name = $name;
		$this->load_styles();
		$this->load_scripts();
	}

	public function load_styles() {
		wp_enqueue_style( $this->name, BSP_PLUGIN_URL . '/dist/main.css', [], filemtime( BSP_PLUGIN_PATH . '/dist/main.css' ), 'screen' );
	}

	public function load_scripts() {
		wp_enqueue_script( $this->name, BSP_PLUGIN_URL . '/dist/main.js', [], filemtime( BSP_PLUGIN_PATH . '/dist/main.js' ), true );
	}

	public function search_template( $template, $type, $templates ) {

		return $template;
	}

	public function template_redirect( $template ) {}
	public function template_include( $template ) {
		if ( is_search() ) {
			return BSP_PLUGIN_PATH . 'templates/search.php';
		}

		return $template;
	}

	public function pre_get_posts( $query ) {
		if ( is_admin() || ! $query->is_main_query() ) {
			return;
		}

		if ( $query->is_search ) {
			$post_types = get_post_types(
				[
					'public' => true,
					// 'publicly_queryable' => true,
					// '_builtin' => true,
				]
			);

			$post_types = array_filter(
				$post_types,
				function( $p ) {
					return ! in_array( $p, [ 'memberpressproduct', 'memberpressgroup', 'memberpressrule' ] );
				}
			);

			$query->set( 'post_type', array_values( $post_types ) );
			$query->set(
				'orderby',
				[
					'post_type' => 'desc',
					'relevance' => 'desc',
					'date'      => 'desc',
				]
			);
			$query->set( 'posts_per_page', 40 );
		}
	}

	public function go() {
		add_filter( 'search_template', [ $this, 'search_template' ], 100, 3 );
		add_filter( 'template_include', [ $this, 'template_include' ], 100, 1 );
		add_action( 'template_redirect', [ $this, 'template_redirect' ], 100, 1 );
		add_action( 'pre_get_posts', [ $this, 'pre_get_posts' ], 100, 1 );
	}
}
