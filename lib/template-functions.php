<?php


function bsp_new_search_section( $first = false ) {

	global $s, $bsp_pt;

	if ( ! isset( $bsp_pt ) ) {
		$bsp_pt = '';
	}

	if ( $bsp_pt === get_post_type() ) {
		return;
	}
	$bsp_pt        = get_post_type();
	$post_type_obj = get_post_type_object( $bsp_pt );

	if ( ! $first ) {
		bsp_close_search_section();
	}

	$html = sprintf(
		'<section class="search-section search-section-%s">
			<h2 class="search-section-title">%s containing "%s"</h2>',
		$bsp_pt,
		$post_type_obj->label,
		$s
	);
	$html = apply_filters( 'bsp_new_search_section', $html );

	echo $html;
}

function bsp_close_search_section() {
	$html = sprintf( '</section>' );
	$html = apply_filters( 'bsp_close_search_section', $html );

	echo $html;
}

function bsp_search_item() {
	global $bsp_pt;

	$templates = [
		'page' => BSP_PLUGIN_PATH . 'templates/page.php',
		'post' => BSP_PLUGIN_PATH . 'templates/post.php',
	];

	if ( isset( $templates[ $bsp_pt ] ) ) {
		$template = $templates[ $bsp_pt ];
	} else {
		$template = array_pop( $templates );
	}

	$n_template = apply_filters( 'bsp_search_item_template', $template, $bsp_pt );

	if ( file_exists( $n_template ) ) {
		include $n_template;
	} else {
		include $template;
	}
}

function bsp_paginate() {
	printf(
		'<div class="search-pages">%s</div>',
		paginate_links()
	);
}
