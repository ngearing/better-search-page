<?php
/**
 * Plugin Name: Better Search Page
 * Description: Overwrites default search page to give a better experience.
 * Author: Nathan
 * Version: 0.0.1
 *
 * @package bsp
 */

if ( ! ABSPATH ) {
	die();
}

$namespace = 'BSP';
$version   = '0.0.1';
$php_min   = '5.6';
$wp_min    = '5';

define( 'BSP_PLUGIN_FILE', __FILE__ );
define( 'BSP_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'BSP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'BSP_PLUGIN_VERSION', $version );

function bsp_test_versions() {
	$tests = [
		'wordpress' => $wp_min,
		'php'       => $php_min,
	];

	foreach ( $tests as $key => $value ) {
		// Test version.
	}
}

require_once BSP_PLUGIN_PATH . 'vendor/autoload.php';

function lets_go() {
	$plugin = new \BSP\BSP( 'better-search-page' );
	$plugin->go();
}
lets_go();
